<?php

namespace App\Form;

use App\Entity\Person;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

//Formulaire généré avec bin/console make:form

class PersonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /*
         Etant donnée que ce Form se base sur une entité Doctrine, 
         symfony se sert par défaut des types indiqués dans les annotations
         de celle ci pour déterminer les types d'input adequat. On peut venir
         lui en donner d'autre si jamais on a des contraintes spécifiques.
         */
        $builder
            ->add('name')
            ->add('surname')
            ->add('birthdate')
            ->add('nationality')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Person::class,
        ]);
    }
}
