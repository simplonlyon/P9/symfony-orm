<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

//Entité obtenue avec la commande bin/console make:entity

/*
Pour utiliser Doctrine, on doit définir des entités avec des annotations. C'est à
partir de celles ci que l'ORM saura quel tables faire, leur structure etc.
Le principe de Doctrine est de ne plus manipuler de SQL du tout, mais uniquement
des entités et l'ORM fera le reste automatiquement.
Lorsqu'on modifie le model, il faut créer un fichier de migration avec la commande
bin/console make:migration
Cette commande crée un script SQL contenant toutes les requêtes nécessaire pour faire
passer l'état de la bdd actuel dans l'état dans lequel se trouve le modèle.
Il faut toujours suivre cette commande d'un bin/console doctrine:migrations:migrate
qui exécutera tous les fichiers de migration existant.
(/!\ Attention, le make:migration se base sur l'état actuel de la bdd, si on fait
deux fois de suite cette commande, il fera deux script sql similaire et lors de leur
exécution dans le migrate, il y aura un conflit entre les deux)

*/

/**
 * @ORM\Entity(repositoryClass="App\Repository\PersonRepository")
 */
class Person
{
    /*
    On indique avec des annotation quelle est la clef primaire de l'entité avec
    le @Orm\Id(), on dit que c'est en autoincrement avec le GeneratedValue
    */
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /*
    On indique avec les annotation @ORM\Column quelles propriétés devront être
    traduite en colonne de table SQL et de quel type chaque colonne devra être
    */
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $surname;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $birthdate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nationality;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Car", mappedBy="person")
     */
    private $cars;

    public function __construct()
    {
        $this->cars = new ArrayCollection();
    }

    /*
     Les entités générées par Doctrine respecte une convention des Objets qui consiste
     à mettre toutes les propriétés de celui ci en private ou protected et d'utiliser
     pour récupérer la valeur d'une propriété un getter (ici getId()) et pour changer
     la valeur d'une propriété un setter (par exemple setName(string $name)).
    L'idée est de pouvoir mettre des contrainte en passant par des méthodes, par exemple
    des contrainte de typage, ou alors de logique (exemple, on ne peut pas avoir un age négatif)
    dans les méthodes. 
    Cela permet également de pouvoir modifier nos propriétés sans casser tout notre code
    en venant juste modifier le contenu du getter pour le faire correspondre aux nouvelles
    propriétés.
     */

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getBirthdate(): ?\DateTimeInterface
    {
        return $this->birthdate;
    }

    public function setBirthdate(?\DateTimeInterface $birthdate): self
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    public function getNationality(): ?string
    {
        return $this->nationality;
    }

    public function setNationality(?string $nationality): self
    {
        $this->nationality = $nationality;

        return $this;
    }

    /**
     * @return Collection|Car[]
     */
    public function getCars(): Collection
    {
        return $this->cars;
    }

    public function addCar(Car $car): self
    {
        if (!$this->cars->contains($car)) {
            $this->cars[] = $car;
            $car->setPerson($this);
        }

        return $this;
    }

    public function removeCar(Car $car): self
    {
        if ($this->cars->contains($car)) {
            $this->cars->removeElement($car);
            // set the owning side to null (unless already changed)
            if ($car->getPerson() === $this) {
                $car->setPerson(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return 'Person ' . $this->id . ' : ' . $this->name . ' ' . $this->surname;
    }
}
