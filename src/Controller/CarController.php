<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Car;
use App\Form\CarType;

class CarController extends AbstractController
{
    /**
     * @Route("/add-car", name="add_car")
     */
    public function index(Request $request, ObjectManager $objectManager)
    {
        $car = new Car();
        $form = $this->createForm(CarType::class, $car);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $objectManager->persist($car);
            $objectManager->flush();

            return $this->redirectToRoute('home');

        }

        return $this->render('car/index.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
