<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Person;
use App\Form\PersonType;
use Doctrine\Common\Persistence\ObjectManager;
use App\Repository\PersonRepository;

class HomeController extends AbstractController
{
    /**
     * Si jamais on utilise un repository ou l'object manager dans plusieurs
     * routes d'un même contrôleur, on peut l'injecter directement dans le 
     * constructeur pour ne pas avoir à le réinjecter dans chaque route, mais
     * on peut aussi juste l'injecter dans chaque route, c'est pas foncièrement mal
     */
    // private $repo;

    // public function __construct(PersonRepository $repo) {
    //     $this->repo = $repo;
    // }

    /*
     On injecte le PersonRepository dans notre route. Avec Doctrine, les
     repositories serviront pour tout ce qui concernera la lecture des données
     Donc tous les find, findAll, findBy etc. seront fait avec le Repository. 
     */
    /**
     * @Route("/", name="home")
     */
    public function index(PersonRepository $repo)
    {
        //ci dessous, une manière alternative de récupérer le répository, il est
        //inutile d'utiliser ça si on l'a déjà mis en argument 
        //$repo = $this->getDoctrine()->getRepository(Person::class);

        return $this->render('home/index.html.twig', [
            'persons' => $repo->findAll()
        ]);
    }
    /*
    Dans cet route, on injecte l'ObjectManager de Doctrine. Cet objet permettra
    de gérer tout ce qui concerne les ajouts/suppression/modification de données
    dans la bdd
    */
    /**
     * @Route("/add-person", name="add_person")
     */
    public function add(Request $request, ObjectManager $manager) {
        //Manière alternative de récupérer l'ObjectManager
        //$manager = $this->getDoctrine()->getManager();
        $person = new Person();
        //on fait notre formulaire classique
        $form = $this->createForm(PersonType::class, $person);
        
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            //On utilise la méthode persist du manager qui fera persister en bdd
            //l'instance d'entité qu'on lui donne
            $manager->persist($person);
            //Pour que les commandes de l'ObjectManager soit exécutée, il faut forcément
            //utiliser la méthode flush() qui les déclenchera (pas de flush, pas d'exécution)
            $manager->flush();

            
            return $this->redirectToRoute('home');
        }

        return $this->render('home/add-person.html.twig', [
            'form' => $form->createView(),
            'verb' => 'Add'
            ]);
    }

    /*
     Ici, on utilise le @ParamConverter de symfony configuré par défaut avec
     Doctrine. On donne un argument dans le chemin de la route et plutôt que de 
     typer notre argument en int dans la fonction, on le type directement dans le
     type d'Entité qu'on veut récupérer, Symfony fera automatiquement le find pour
     aller chercher l'instance correspondante avec Doctrine.
     Comme nom de paramètre de route on peut soit mettre id et il devinera qu'on veut
     l'id de l'entité Person (marche si ya qu'une seule entité à choper), soit mettre
     directement le même nom que l'argument de la fonction si jamais ya ambiguité.
     Si jamais le ParamConverter ne trouve pas l'entité, il renverra automatiquement
     une 404, ce qui est bien.
     */
    /**
     * @Route("/person/{person}", name="one_person")
     */
    public function onePerson(Person $person) {
        
        return $this->render('home/one-person.html.twig', [
            'person' => $person
        ]);
    }


    /**
     * @Route("/remove-person/{person}", name="remove_person")
     */
    public function removePerson(Person $person, ObjectManager $manager) {
        //On utilise le remove de l'object manager et on lui donne directement la 
        //personne récupérée par le ParamConverter
        $manager->remove($person);
        $manager->flush();
        /**
         * Ici, on choisit de faire une redirection une fois la personne supprimée
         * On aurait pu également faire un render d'un template spécifique et le but
         * de ce template aurait été de mettre un message d'info en mode "vous avez
         * bien supprimée la personne. Cliquez ici pour revenir à l'accueil"
         */
        return $this->redirectToRoute('home');
    }
    
    /**
     * Ici on fait 2 fonctionnalités en une, l'ajout et la modification. Pour ce
     * faire on fait une route avec un paramètre, mais on met l'argument de la 
     * fonction qui lui est lié avec une valeur par défaut, le paramètre de la route
     * devient ainsi optionnel. Dans un cas, on sera allé sur /modify-person et la
     * variable person sera null, dans l'autre cas, on sera allé sur /modify-person/1 par ex
     * et à ce moment, la variable person contiendra la person de la bdd correspondante à cet id
     */
     /**
     * @Route("/modify-person/{person}", name="modify_person")
     */
    public function modify(Request $request, ObjectManager $manager, Person $person = null) {
        //Variable qui sera utilisé pour l'affichage selon si c'est un ajout ou une modif
        $verb = 'Modify';
        //Si la person n'existe pas (genre qu'elle est null), on est en mode ajout
        if(!$person) {
            //on crée une nouvelle instance...
            $person = new Person();
            //...et on lui change le verbe pour mettre Add
            $verb = 'Add';
        }
        //On fait le reste de la méthode comme le add de base
        $form = $this->createForm(PersonType::class, $person);
        
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $manager->persist($person);
            $manager->flush();

            
            return $this->redirectToRoute('home');
        }

        return $this->render('home/add-person.html.twig', [
            'form' => $form->createView(),
            'verb' => $verb
            ]);
    }

}
